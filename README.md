## expressly angular 1.4 browserify seed

### Prerequisites
- node 0.12
- npm
- bower
- gulp

### Running
1. run `npm install`
2. run `bower install`
3. run `gulp`
4. Page will be served on port `1337`; navigate to `localhost:1337`